(function () {
    'use strict';

    $(window).on("load", function () {

        const $body = $('body');
        const $header = $('.header');
        let scrollPos = 0;

        $('.js-hamburger').on('click', function () {
            const $this = $(this);

            $this.toggleClass('is-active');
            $body.toggleClass('open-drop-menu');

            if ($this.hasClass('is-active')) {
                $body.css('overflow', 'hidden');
            } else {
                $body.css('overflow', 'initial');
            }
        });

        $('.nav__item').on('click', function () {
            const $this = $(this);

            if ($this.find('.nav__drop-list').hasClass('opened')) {
                $this.find('.nav__drop-list').slideUp(350).removeClass('opened');
            } else {
                $('.nav__item').find('.nav__drop-list').slideUp(350).removeClass('opened');
                $this.find('.nav__drop-list').slideDown(350).addClass('opened');
            }
        });


        window.addEventListener('scroll', function(){

            detectionDirection(handlerDirectionUp, handlerDirectionDown)

        });

        function detectionDirection(funcUp, funcDown) {
            if ((document.body.getBoundingClientRect()).top > scrollPos)
                funcDown();
            else
                funcUp();

            scrollPos = (document.body.getBoundingClientRect()).top;
        }

        function handlerDirectionUp() {
            $header.addClass('sticky');
        }

        function handlerDirectionDown() {
            $header.removeClass('sticky');
        }

    });

})();
